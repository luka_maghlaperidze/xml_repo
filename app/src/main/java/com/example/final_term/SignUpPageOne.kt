package com.example.final_term

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_sign_up_page_one.*

class SignUpPageOne : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_page_one)
        init()
    }
    private fun init(){
        signUpNextButton.setOnClickListener {
            startActivity(Intent(this,SignUpPageTwo::class.java))
        }
    }
}