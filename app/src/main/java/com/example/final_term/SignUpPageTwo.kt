package com.example.final_term

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_sign_up_page_two.*

class SignUpPageTwo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_page_two)
        init()
    }
    private fun init(){
        finishButton.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
        }
    }
}